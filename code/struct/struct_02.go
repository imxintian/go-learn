package main

import "fmt"

type person struct {
	name string
	age  int
}

func foo() {
	//如果一个结构体值是可寻址的，则它的字段也是可寻址的；
	//反之，一个不可寻址的结构体值的字段也是不可寻址的。
	//不可寻址的字段的值是不可更改的。所有的组合字面量都是不可寻址的。
	james := person{name: "James", age: 20}
	p := &james.age
	*p = 30
	fmt.Println(james)

}

func main() {
	foo()
}
