package main

import "fmt"

// Declaration

type Book struct {
	title  string
	author string
	pages  int
}

func main() {
	book := Book{"Go Programming", "Mahesh Kumar", 200}
	fmt.Println(book)
	// 使用带字段名称的组合字面量来表示结构体值
	book2 := Book{title: "Go Programming", author: "Mahesh Kumar", pages: 200}
	fmt.Println(book2)
	// title和author字段的值都为空字符串""，pages字段的值为0。
	book3 := Book{}
	fmt.Println(book3)
	// title字段空字符串""，pages字段为0。
	book4 := Book{author: "Mahesh Kumar"}
	fmt.Println(book4)
	//使用选择器来访问结构体字段
	fmt.Println(book.title)
	fmt.Println(book.author)
	fmt.Println(book.pages)
}
