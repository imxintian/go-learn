package main

import "fmt"

//指针
// 一个非定义指针类型的字面形式为*T，其中T为一个任意类型。类型T称为指针类型*T的基类型（base type）。
//如果一个指针类型的基类型为T，则我们可以称此指针类型为一个T指针类型。
// 如果一个指针类型的底层类型是*T，则它的基类型为T。
//如果两个非定义指针类型的基类型为同一类型，则这两个非定义指针类型亦为同一类型。

func main() {
	// *int  // 一个基类型为int的非定义指针类型。
	// **int // 一个多级非定义指针类型，它的基类型为*int。

	type Ptr *int // Ptr是一个定义的指针类型，它的基类型为int
	type pp *Ptr  // pp是一个定义的多级指针类型，它的基类型为Ptr

	p0 := new(int)         //p0是一个int类型的非定义指针
	fmt.Println(p0)        //（打印出一个十六进制形式的地址）
	fmt.Printf("%T\n", p0) // *int
	fmt.Println(*p0)
	x := *p0
	fmt.Println(x) // 0
	p1, p2 := &x, &x
	fmt.Println(p1, p2)   // 0x8181818181818180 0x8181818181818180
	fmt.Println(*p1, *p2) // 0 0
}
