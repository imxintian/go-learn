package main

import "fmt"

// 数组
// 数组的声明及初始化
var arr1 [10]int
var arr2 = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

var arr3 = [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

var arr4 = [...]int{ //将第8个元素赋值为8 将第9个元素赋值为9 将第10个元素赋值为10 其余元素值为0
	8: 8, 9: 9, 10: 10}

// 多维数组
var arr5 = [2][3]int{
	{1, 2, 3},
	{4, 5, 6},
}

func main() {
	// 打印
	fmt.Println(arr1)
	fmt.Println(arr2)
	fmt.Println(arr3)
	fmt.Println(arr4)
	fmt.Println(arr5)
	// 数组的遍历
	for i := 0; i < len(arr2); i++ {
		fmt.Println(arr2[i])
	}
	fmt.Println("-----分割线-----")
	// 多维数组的遍历
	for i := 0; i < len(arr5); i++ {
		for j := 0; j < len(arr5[i]); j++ {
			fmt.Println(arr5[i][j])
		}
	}
}
