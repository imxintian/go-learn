package main

import "fmt"

// 映射
// 映射类型
// 映射类型：map[K]T
// 映射类型的元素是键值对，其中 K必须为一个可比较类型。它指定了一个映射类型的键值类型。
//T可为任意类型。它表示一个容器类型的元素类型。某个特定容器类型的值中只能存储此容器类型的元素类型的值。

func main() {
	// 创建一个映射
	// 创建一个映射，键值类型为string，值类型为int
	var m1 map[string]int
	// 创建一个映射，键值类型为string，值类型为int
	m2 := make(map[string]int)
	// 创建一个映射，键值类型为string，值类型为int 容量为10
	m3 := make(map[string]int, 10)
	// 创建一个映射，键值类型为string，值类型为int
	m4 := map[string]int{}

	// 创建一个映射，键值类型为string，值类型为int
	m5 := map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9, "j": 10}
	// 创建一个映射，键值类型为string，值类型为int
	m6 := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
		"e": 5,
	}

	fmt.Println(m1)
	fmt.Println(m2)
	fmt.Println(m3)
	fmt.Println(m4)
	fmt.Println(m5)
	fmt.Println(m6)

	// 判断 m5 是否存在某个键,存在遍历，不存在返回false
	if v, ok := m5["a"]; ok {
		fmt.Println(v)
	} else {
		fmt.Println("不存在")
	}
	// 遍历m6
	for k, v := range m6 {
		fmt.Println(k, v)
	}

}
