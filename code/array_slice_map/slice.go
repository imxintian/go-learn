package main

import (
	"fmt"
)

// 声明一个切片
//数组声明相比，切片声明仅仅是少了一个“长度”属性
// 切片的长度和容量都是可以改变的
var nums = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

// 通过make函数创建切片
// make([]T, len, cap)
var nums2 = make([]int, 10, 20)

// 切片的长度和容量都是可以改变的
var nums3 = make([]int, 10)

// 基于数组创建切片
var nums4 = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
var nums5 = nums[0:5]

func main() {
	// 打印nums的类型
	fmt.Printf("nums type: %T\n", nums)
	// 打印nums的长度
	fmt.Printf("nums length: %d\n", len(nums))
	// 打印nums的容量
	fmt.Printf("nums capacity: %d\n", cap(nums))
	// 打印nums2
	fmt.Printf("nums2: %v\n", nums2)
	// 打印nums3
	fmt.Printf("nums3: %v\n", nums3)
	// 打印nums4
	fmt.Printf("nums4: %v\n", nums4)
	// 打印nums5
	fmt.Printf("nums5: %v\n", nums5)
	// 打印nums5的长度 high-low
	fmt.Printf("nums5 length: %d\n", len(nums5))
	// 打印nums5的容量 max-min
	fmt.Printf("nums5 capacity: %d\n", cap(nums5))
	// 对nums5进行扩容
	nums5 = append(nums5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	// 打印nums5
	fmt.Printf("nums5: %v\n", nums5)
	// 打印nums5的长度 high-low
	fmt.Printf("nums5 length: %d\n", len(nums5))
	// 打印nums5的容量 2cap
	fmt.Printf("nums5 capacity: %d\n", cap(nums5))
}
