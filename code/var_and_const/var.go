package main

//声明变量
var a = 10

// 批量声明变量
var (
	b = 20
	c = 30
)

//变量初始化
var name string = "张三"
var age int = 20

// 一次初始化多个变量
var (
	name1 string = "张三"
	age1  int    = 20
)

// 类型推导

var name3, age3 = "张三", 20

func person() (string, int) {
	// 在函数内部，可以使用更简略的 := 方式声明并初始化变量
	name4 := "张三"
	age4 := 20
	return name4, age4
}

func main() {
	// 匿名变量用一个下划线_表示
	_, age5 := person()
	println(age5)
	println(a, b, c, name, age, name1, age1, name3, age3)
	println(person())
}
