package main

// const declares a constant value

const P float64 = 3.1415926

const internal = "包内可访问"
const External = "包外可访问"

const (
	n1 = iota //0
	n2        //1
	n3        //2
	_
	n4 //
)

func main() {
	println(internal)
	println(External)
	println(P)
	println(n1)
	println(n2)
	println(n3)
	println(n4)

}
